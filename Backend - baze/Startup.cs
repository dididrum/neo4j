﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNet.OData;
using Microsoft.OData.Edm;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Builder;
using Projekat.Models;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
using Newtonsoft.Json;
using Hangfire;
using Hangfire.SqlServer;
using Neo4j.Driver;

namespace Projekat
{
    public class Startup
    {
        //MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
       // readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          //  services.AddCors(options => {​​​​​ options.AddPolicy(name: MyAllowSpecificOrigins, builder => {​​​​​ builder.WithOrigins("http://example.com", "http://www.contoso.com"); }​​​​​); }​​​​​);
            services.AddSingleton(GraphDatabase.Driver("bolt://localhost:7687", AuthTokens.Basic("neo4j", "sifra")));
            services.AddDbContext<VinylContext>(options =>
            {
                options.UseLazyLoadingProxies();
               
            });
            services.AddControllers(options => options.EnableEndpointRouting=false)
            .AddNewtonsoftJson(options =>{
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.Formatting = Formatting.Indented;
                options.SerializerSettings.MaxDepth = 1;
            });
            services.AddMvc().AddJsonOptions(options =>{
                options.JsonSerializerOptions.WriteIndented=true;
                options.JsonSerializerOptions.PropertyNamingPolicy=null;
                options.JsonSerializerOptions.MaxDepth = 1;
            }).AddXmlSerializerFormatters();
            services.AddCors(options =>{
                options.AddPolicy("LocalhostAllow", builder =>{
                    builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                    //.WithOrigins
                    //(
                    //    "https://localhost:5001",
                    //    "http://localhost:8080"
                    //);
                });
            }); //



            

            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

          

            app.UseMvc();

            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseCors("LocalhostAllow");

           
        }
    }
}
