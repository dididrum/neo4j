﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Projekat.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel;
using System.Net;
using System.Net.Http;
using System.IO;
using Microsoft.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Cors;
using Hangfire;
using Neo4j.Driver;


namespace Projekat.Controllers
{
    
    [EnableCors("LocalhostAllow")]
    [ApiController]
    [Route("[controller]")]
    public class VinylController : Controller { 
        private static IDriver _driver ;
        private static ISession _session = null;

       

        public static ISession GetSession() //kreira sesiju samo prvi put kad pokrene
        {
            if (_session == null)
            {
                _session = _driver.Session();
            }

            return _session;
        }
        private VinylContext vinylContext;

        public VinylController(IDriver driver)
        {
            _driver = driver;
        }
        ///////////////////////////////////////////////////GET

 
        [AllowAnonymous]
        [HttpGet]
        [Route("Records")]
        public ActionResult<List<Object>> GetRecords()
        {

            var session = GetSession();     

            var pom = session.WriteTransaction(t =>  
            {
                var rezultat = t.Run(@"MATCH (n:Record) RETURN n").ToList();
                
                return rezultat;
                

            });

            List<Object> lo = new List<Object>();

            foreach (var t in pom)
            {
                
                var node = t["n"].As<INode>();
         

                var author = node["author"].As<string>();
                var id = node["id"].As<string>();
                var category = node["category"].As<string>();
                var name = node["name"].As<string>();
                var title = node["info"].As<string>();
                var demo = node["demo"].As<string>();
                var komentari = (@"MATCH(n { name: name }) -[r: ListaKomentara]->(c) RETURN r").ToList();
               
                lo.Add(new {id=id, name = name, title = title, author=author, category=category, demo =demo, comments=komentari });
            }
            return lo;
           
          //  return vinylContext.Records.ToList();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("Song")]
        public ActionResult<List<Object>> GetSong()
        {

            var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"MATCH (n:Song) RETURN n").ToList();

                return rezultat;
            });

            List<Object> lo = new List<Object>();
            foreach (var t in pom)
            {
                var node = t["n"].As<INode>();

                var recordref = node["recordref"].As<string>();
                var title = node["title"].As<string>();
                lo.Add(new { recordref = recordref, title = title });
            }

            return lo;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("Comment")]
        public ActionResult<List<Object>> GetComment()
        {

            var session = GetSession();

            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"MATCH (n:Comment) RETURN n").ToList();

                return rezultat;
            });

            List<Object> lo = new List<Object>();
            foreach (var t in pom)
            {
                var node = t["n"].As<INode>();

                var boundrecordref = node["boundrecordref"].As<string>();
                var content = node["content"].As<string>();
                lo.Add(new { boundrecordref = boundrecordref, content = content });

            }

            return lo;
        }



        [AllowAnonymous]
        [HttpGet]
        [Route("RecordsFrom/{kategorija}")]
        public ActionResult<Object> GetRecord(string kategorija)
        {
            var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"MATCH (n:Record) WHERE n.category='" + kategorija + "' RETURN n ").ToList();
                return rezultat;

            });

            List<Object> lo = new List<Object>();
            foreach (var t in pom)
            {
                var node = t["n"].As<INode>();
                var id = node["id"].As<string>();

                var author = node["author"].As<string>();
                var name = node["name"].As<string>();
                var info = node["info"].As<string>();
                var demo = node["demo"].As<string>();
                var category = node["category"].As<string>();


                lo.Add(new { id = id, name = name, info = info, author = author, category = category, demo = demo });
            }
            return lo;


            // return vinylContext.Records.Find(RecordId);
        }


        [AllowAnonymous]
        [HttpGet]
        [Route("Record/{RecordId}")]
        public ActionResult<Record> GetRecord(int RecordId)
        {
            var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"MATCH (n:Record) WHERE n.id='" + RecordId.ToString() + "' RETURN n ").ToList();
                return rezultat;
               
            });

            Record lo = new Record();
            foreach (var t in pom)
            {
                var node = t["n"].As<INode>();
                var id = node["id"].As<int>();

                var author = node["author"].As<string>();
                var name = node["name"].As<string>();
                var info = node["info"].As<string>();
                var demo = node["demo"].As<string>();
                var category = node["category"].As<string>();


                lo.Id = id;
                lo.Name = name;
                lo.Info = info;
                lo.Author = author;
                lo.Category = category;
                lo.Demo = demo;
            }
            return lo;


            // return vinylContext.Records.Find(RecordId);
        }



        ///////////////////////////////////////////////////POST

        [HttpPost]
        [Route("Add/Record")]
        public IActionResult PostAddRecord([FromBody] Record r)
        {
            var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"CREATE (n:Record { author: '" + r.Author + "', name: '" + r.Name + "', category: '" + r.Category + "', id: '" + r.Id + "', demo: '" + r.Demo + "',  info: '" + r.Info + "'}) return n").ToList();
                return rezultat;

            });

            return Ok();
        }

        [HttpPost]
        [Route("Add/Comment")]
        public IActionResult PostAddComment([FromBody] Comment c)
        {

            var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"CREATE (n:Record { content: '" + c.Content + "', id: '" + c.Id + "'}) return n").ToList();
                return rezultat;

            });

            return Ok();
        }



        /*  [AllowAnonymous]  
          [HttpGet]
          [Route("RecordImg/{RecordName}")]
          public ActionResult GetRecordImg(string RecordName)
          {
              Record record;
              if((record = vinylContext.Records.SingleOrDefault(r => r.Name == RecordName)) == null || record.DisplayImg == null)
                  return NotFound();
              string path = Constants._rootPathRecords + RecordName + "\\" + record.DisplayImg;
              string ex = Path.GetExtension(path);
              string type =  "image/" + ex;

              IFileProvider provider = new PhysicalFileProvider(Constants._rootPathRecords + RecordName);
              IFileInfo fileInfo = provider.GetFileInfo(record.DisplayImg);
              var readStr = fileInfo.CreateReadStream();

              return File(readStr, type, RecordName + "_" + record.DisplayImg);
          }

         */


        ///////////////////////////////////////////////JWTRelated   //ovo
      //  private static string ClaimsIdentifier = ClaimTypes.NameIdentifier;
     //   private static string ClaimsRole = ClaimTypes.Role;



        /*


   ///////////////////////////////////////////////////////////POST_IMG
   ///

           [HttpPost]
           [Route("Add/RecordImg/{RecordName}")]
           public async Task<IActionResult> POSTAddRecordImg(string RecordName, [FromForm]ImageUploadModel image)
           {
               if(!ModelState.IsValid)
                   return BadRequest();
               if(image == null)
                   return BadRequest();
               Record record;
               if((record = vinylContext.Records.SingleOrDefault(r => r.Name == RecordName)) == null)
                   return NoContent();
               string role = User.Claims.SingleOrDefault(u => u.Type == ClaimsRole).Value;

               if(!Directory.Exists(Constants._rootPathRecords + "//" + RecordName + "//"))
                   Directory.CreateDirectory(Constants._rootPathRecords + "//" + RecordName + "//");
               if(record.DisplayImg != null)
                   System.IO.File.Delete(Constants._rootPathRecords + "//" + RecordName + "//" + record.DisplayImg);
               if(image.Image.Length > 0)
               {
                   using(FileStream fs = System.IO.File.Create(Constants._rootPathRecords + "//" + RecordName + "//" + image.Image.FileName))
                   {
                       await image.Image.CopyToAsync(fs);
                       await fs.FlushAsync();
                   }
                   record.DisplayImg = image.Image.FileName;
                   vinylContext.Records.Update(record);
                   vinylContext.SaveChanges();
                   return Ok();
               }
               return BadRequest();
           }



   ///////////////////////////////////////////////////////////PUT

        */
           [HttpPut]
           [Route("Update/Record")]
           public IActionResult PutUpdateRecord(Record r)
           {
               var session = GetSession();
               var pom = session.WriteTransaction(t =>
               {
                   var rezultat = t.Run(@"MATCH (n:Record) WHERE n.id='" + r.Id + "' SET n.info = '" + r.Info + "'").ToList();
       
                   return rezultat;

               });

               return Ok();
             
           }


           [HttpPut]
           [Route("Update/Comment")]
           public ActionResult PutUpdateComment([FromBody]Comment c)
           {
               if(!ModelState.IsValid)
                   return BadRequest();
               Comment comment;
               if((comment = vinylContext.Comments.Find(c.Id)) == null)
                   return NotFound();

               comment.Content = c.Content;
               vinylContext.SaveChanges();
               return Ok();
           }

   ////////////////////////////////////////////////////DELETE
       

        [HttpDelete]
           [Route("Delete/Record/{Naziv}")]
           public IActionResult DeleteRecord(string Naziv)
           { 

        var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"MATCH (n:Record) WHERE n.name='" + Naziv + "' DELETE n ").ToList();
                return rezultat;

            });
            return Ok();

        }


        [HttpDelete]
        [Route("Delete/Comment/{CommentId}")]
        public IActionResult DeleteComment(int CommentId)
        {
         
            var session = GetSession();
            var pom = session.WriteTransaction(t =>
            {
                var rezultat = t.Run(@"MATCH (n:Comment) WHERE n.id='" + CommentId.ToString() + "' DELETE n ").ToList();
                return rezultat;

            });
            return Ok();

        }



            /*

            /////////////////////////////////////////////DeleteHelpers

                      private void helpDeleteRecord(Record r)
                      {
                          List<Comment> recordComments = r.Comments.ToList();
                          foreach (Comment e in recordComments)
                          {
                              vinylContext.Comments.Remove(e);
                          }
                          foreach(Song s in r.Songs.ToList())
                          {
                              vinylContext.Songs.Remove(s);
                          }
                          vinylContext.Records.Remove(r);
                      }

                      */
        }
    }