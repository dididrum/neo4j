using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekat.Models
{
    [Table("Song")]
    public class Song
    {
        public int Id { get; set; }
        public int RecordRef { get; set; }

        public virtual Record Record { get; set; }

        public string Title { get; set; }
    }
}