
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekat.Models
{
    [Table("Record")]
    public class Record
    {
        public int Id { get; set; }
     
        public string Name { get; set; }
     
        public string Info { get; set; }
        
        public string Category { get; set; }
       
        public string Author { get; set; }

        public string Demo { get; set; }

      
    }
}