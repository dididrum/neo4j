using Microsoft.EntityFrameworkCore;

namespace Projekat.Models
{
    public class VinylContext : DbContext
    {
        public VinylContext(DbContextOptions<VinylContext> options):base(options){}

      
        public DbSet<Record> Records { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<Comment> Comments { get; set; }

        
    }
}