using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projekat.Models
{
    [Table("Comment")]
    public class Comment
    {
        public int Id { get; set; }

        public string Content { get; set; }
    }
}