import Vue from 'vue'
//import App from './App'

import VueRouter from 'vue-router'
import Gallery from './views/Gallery'
import Vinyl from './components/Vinyl'

Vue.use(VueRouter)

const routes = new VueRouter({
  base: process.env.BASE_URL,
  mode: 'history',
  duplicateNavigationPolicy: 'ignore',
  routes: [
    {
      path: '/',
      name: 'Gallery',
      component: Gallery
    },
    {
      path: '/vinyl/:id',
      name: 'Vinyl',
      component: Vinyl,
      props:true
    }
  
  ]
})

routes.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem('access_token')

  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next()
  }
  next()
})

export default routes
